import 'package:flutter/material.dart';

class FeatureScreen1 extends StatelessWidget {
  const FeatureScreen1({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Feature Screen1'),
      ),
             bottomNavigationBar: BottomAppBar(
          shape: const CircularNotchedRectangle(),
          child: Container(height: 50.0,),
             ), 
                     floatingActionButton: FloatingActionButton(
          onPressed: () => {print("plus button is clicked")},
          child: const Icon(Icons.add),
                     ),
    );
  }
}