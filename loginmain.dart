import 'package:flutter/material.dart';
import 'package:flutter_application_1/dashboard.dart';
import 'package:flutter_application_1/main.dart';
import 'package:flutter_application_1/signup.dart';

class LogIN extends StatelessWidget {
  const LogIN({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       body: SafeArea(
         child: Container(
width: double.infinity,
height: MediaQuery.of(context).size.height,
padding: EdgeInsets.symmetric(horizontal: 30,vertical: 400),
child: Column(
  mainAxisAlignment: MainAxisAlignment.spaceBetween,
  crossAxisAlignment: CrossAxisAlignment.center,
  children: <Widget>[
     Column(
        children: <Widget>[
          MaterialButton(
            minWidth: double.infinity,
            height: 60,
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context)=> Registration()));
            },
            
            shape: RoundedRectangleBorder(
              side: BorderSide(
                color: Colors.black
              ),
              borderRadius: BorderRadius.circular(50)
            ),
            child: Text(
              'LogIn',
              style: TextStyle(
                fontWeight:FontWeight.w600
              ),
              ),
           )
         ] 
          ),
          SizedBox(height: 20,),
          MaterialButton(
            minWidth: double.infinity,
            height: 80,
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context)=> SignUp()));
            },
            color: Color(0xff0095FF),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Text(
              "SignUp",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w600,
                fontSize: 18,
              ),),
            )
         ],
         ),
         )
         ),
      
            
      
        
        );
      
    
    
  }
}