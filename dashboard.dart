import 'package:flutter/material.dart';
import 'package:flutter_application_1/featurescreen1.dart';
import 'package:flutter_application_1/featurescreen2.dart';
class Registration extends StatelessWidget {
  const Registration({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dashboard'),
      ),
     body: Center(
       child: Column(
         children: <Widget>[
           Row(
             mainAxisAlignment: MainAxisAlignment.center,
             children: <Widget>[
               Padding(
                 padding: EdgeInsets.all(20.0),
                 child: new MaterialButton(
                   height: 100.0,
                   minWidth: 150.0,
                   color: Theme.of(context).primaryColor,
                   textColor: Colors.white,
                   child: Text("Feature Screen1"),
                   onPressed: () => {
                     Navigator.push(context, MaterialPageRoute(builder: (context) => FeatureScreen1())),
                   },
                 ),
                 ),
                  Padding(
                 padding: EdgeInsets.all(20.0),
                 child: new MaterialButton(
                   height: 100.0,
                   minWidth: 150.0,
                   color: Theme.of(context).primaryColor,
                   textColor: Colors.white,
                   child: Text("Feature Screen2"),
                   onPressed: () => {
                     Navigator.push(context, MaterialPageRoute(builder: (context) => FeatureScreen2())),
                   },
                 ),
                 ),
             ],
           )
         ],
       ),
     ),
       
        bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        child: Container(height: 50.0,),
         ),
          
          floatingActionButton: FloatingActionButton(
          onPressed: () => {print("plus button is clicked")},
          child: const Icon(Icons.add),
            ),
    
    );
  }
}
